﻿/// <summary>
/// Class này dùng để hiển thị và xử lý dữ liệu từ người dùng yêu cầu trong ứng dụng quản lý khách sạn.
/// </summary>
public class HotelUI
{
    // Đối tượng Hotel để thực hiện các thao tác quản lý khách sạn
    public Hotel myHotel { get; set; }

    // Constructor, khởi tạo đối tượng HotelUI và liên kết với Hotel
    public HotelUI()
    {
        myHotel = new Hotel("123 Main Street", "555-1234");
    }

    // Hàm thêm phòng mới vào khách sạn
    public void AddRoom()
    {
        var room = new Room();
        // TODO: Nhập thông tin và Thêm phòng mới
        myHotel.AddRoom(room);
    }

    // Hàm xóa phòng khỏi khách sạn
    public void RemoveRoom()
    {
        // TODO: Nhập thông tin phòng muốn xóa
        myHotel.RemoveRoom(1);
    }

    // Hàm hiển thị tất cả các phòng trong khách sạn
    public void ViewAllRoomInHotel()
    {
        Console.WriteLine("Danh sách tất cả các phòng trong khách sạn:");
        var allRooms = myHotel.GetAllRoomInHotel();
    }

    // Hàm hiển thị thông tin của một phòng
    public void DisplayRoomInfo(Room room)
    {
        // Xuống dòng sau khi hiển thị tất cả các tiện nghi
    }

    // Tìm phòng có thể đặt trong khoảng thời gian
    public void FindARoom()
    {
        Console.Write("Nhập ngày nhận phòng (yyyy-MM-dd): ");
        DateTime checkinDate = DateTime.Parse(Console.ReadLine());
        Console.Write("Nhập ngày trả phòng (yyyy-MM-dd): ");
        DateTime checkoutDate = DateTime.Parse(Console.ReadLine());
        var availableRoom = myHotel.GetAvailableRoom(checkinDate, checkoutDate);
        Console.WriteLine("Hiển thị room lên màn hình console và cho người dùng chọn phòng");
        // TODO: Thêm logic để đặt phòng
        myHotel.BookARoom(1, checkinDate, checkoutDate);
    }

    // Hàm hiển thị báo cáo doanh thu cho một ngày cụ thể
    public void ViewRevenueReport()
    {
        Console.Write("Nhập ngày muốn xem báo cáo (yyyy-MM-dd): ");
        DateTime date = DateTime.Parse(Console.ReadLine());
        var data = myHotel.RevenueReport(date);
        // TODO: hiển thị doanh thu cho ngày cụ thể ở đây
        Console.WriteLine("Hiển thị dữ liệu lấy được lên màn hình");
    }

    // Hàm hiển thị báo cáo doanh thu toàn bộ lịch sử
    public void ViewRevenueHistoryReport()
    {
        Console.WriteLine("Báo cáo doanh thu theo ngày");
        // TODO: Thêm logic để hiển thị doanh thu trong toàn bộ lịch sử
        var data = myHotel.RevenueHistoryReport();
        Console.WriteLine("Hiển thị dữ liệu lấy được lên màn hình");
    }
}
