﻿public class Room
{
    public int RoomNumber { get; set; }
    public RoomType RoomType { get; set; }
    public decimal NightlyRate { get; set; }
    public string[] Amenities { get; set; }
}
