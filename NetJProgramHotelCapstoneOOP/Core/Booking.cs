﻿public class Booking
{
    public int BookingId { get; set; }
    public int RoomNumber { get; set; }
    public DateTime CheckInDate { get; set; }
    public DateTime CheckOutDate { get; set; }
    public string CustomerName { get; set; }
    public decimal TotalRoomCharge { get; set; }

    public AdditionalService[] Services { get; set; }
}

public class AdditionalService
{
    public string Name { get; set; }
    public int Quantity { get; set; }
    public decimal Price { get; set; }
}
