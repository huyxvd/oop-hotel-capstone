﻿public class RevenueReport
{
    public DateTime Date { get; set; }
    public decimal TotalAmount { get; set; }
}
