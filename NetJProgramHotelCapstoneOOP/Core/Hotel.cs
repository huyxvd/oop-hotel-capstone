﻿using Newtonsoft.Json;

public class Hotel
{
    public Room[] Rooms { get; set; }
    public Booking[] Bookings { get; set; }
    public string Address { get; set; }
    public string Phone { get; set; }

    public Hotel(string address, string phone)
    {
        Address = address;
        Phone = phone;
        try
        {
            // Đọc nội dung từ tệp JSON
            string jsonBooking = File.ReadAllText(@"FakeData/Bookings.json");
            string jsonRoom = File.ReadAllText(@"FakeData/Rooms.json");

            // Parse JSON thành mảng đối tượng
            Rooms = JsonConvert.DeserializeObject<Room[]>(jsonRoom) ?? new Room[100];
            Bookings = JsonConvert.DeserializeObject<Booking[]>(jsonBooking) ?? new Booking[100];
        } catch {
            Rooms = new Room[10]; // khởi tạo số lượng room trong khách sạn
            Bookings = new Booking[100]; // khởi tạo số lượng booking(đặt phòng) trong khách sạn
        }
    }

    // Thêm phòng mới vào danh sách
    public void AddRoom(Room room)
    {
        // TODO: Thêm logic thêm phòng vào danh sách
    }

    // Xóa phòng khỏi danh sách theo số phòng
    public void RemoveRoom(int roomNumber)
    {
        // TODO: Thêm logic xóa phòng khỏi danh sách
    }

    // Lấy danh sách tất cả các phòng trong khách sạn
    public Room[] GetAllRoomInHotel()
    {
        return Rooms;
    }

    // Lấy danh sách phòng có thể đặt trong khoảng thời gian cho trước
    public Room[] GetAvailableRoom(DateTime checkin, DateTime checkout)
    {
        // TODO: Thêm logic lấy danh sách phòng có thể đặt
        throw new NotImplementedException();
    }

    // Đặt phòng theo mã phòng và khoảng thời gian
    public void BookARoom(int roomId, DateTime checkin, DateTime checkout)
    {
        // TODO: Thêm logic đặt phòng
        throw new NotImplementedException();
    }

    // Báo cáo doanh thu cho một ngày cụ thể
    public RevenueReport RevenueReport(DateTime date)
    {
        // TODO: Thêm logic báo cáo doanh thu
        throw new NotImplementedException();
    }

    // Báo cáo doanh thu toàn bộ lịch sử
    public RevenueReport[] RevenueHistoryReport()
    {
        // TODO: Thêm logic báo cáo doanh thu toàn bộ lịch sử
        throw new NotImplementedException();
    }
}
