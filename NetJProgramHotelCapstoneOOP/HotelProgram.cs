﻿public class HotelProgram
{
    public void Menu()
    {
        Console.WriteLine("----------- Menu -----------");
        Console.WriteLine("1. Hiển thị Danh Sách Phòng");
        Console.WriteLine("2. Thêm Phòng Mới");
        Console.WriteLine("3. Xóa Phòng");
        Console.WriteLine("4. Tìm Phòng");
        Console.WriteLine("5. Báo Cáo Doanh Thu Ngày");
        Console.WriteLine("6. Báo Cáo Lịch sử doanh thu");
        Console.WriteLine("7. Thoát");
        Console.WriteLine("-----------------------------");
    }
    public void Run()
    {
        HotelUI ui = new HotelUI();

        while (true)
        {
            Menu();
            Console.Write("Nhập lựa chọn của bạn: ");
            string choice = Console.ReadLine();

            switch (choice)
            {
                case "1":
                    ui.ViewAllRoomInHotel();
                    break;
                case "2":
                    ui.AddRoom();
                    break;
                case "3":
                    ui.RemoveRoom();
                    break;
                case "4":
                    ui.FindARoom();
                    break;
                case "5":
                    ui.ViewRevenueReport();
                    break;
                case "6":
                    ui.ViewRevenueHistoryReport();
                    break;
                case "7":
                    Console.WriteLine("Tạm biệt!");
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Lựa chọn không hợp lệ. Vui lòng chọn lại.");
                    break;
            }

            Console.WriteLine("Nhấn Phím bất kỳ để tiếp tục...");
            Console.ReadLine();
            Console.Clear();
        }
    }
}
