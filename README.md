Hệ thống Quản lý Khách sạn
==========================

Bạn được yêu cầu phát triển một ứng dụng console để quản lý thông tin của một khách sạn. Các vấn đề cần phải giải quyết như sau:

1.  Quản lý thông tin phòng khách sạn.
2.  Quản lý đặt phòng trong khách sạn.
3.  Thống kê doanh thu.

### Class Room (Phòng):

-   Properties:
    -   `RoomNumber`: Số phòng để phân biệt giữa các phòng.
    -   `RoomType`: Loại phòng (Single, Double, Family).
    -   `NightlyRate`: Giá tiền mỗi đêm.
    -   `Amenities`: Các tiện nghi trong phòng (wifi, TV, minibar).

### Class Hotel (Khách sạn):

-   Properties:
    -   `Rooms`: Danh sách các phòng trong khách sạn.
    -   `Booking`: Danh sách các phòng được đặt
-   Behavior:
    -   `AddRoom(Room room)`: Thêm một phòng mới vào danh sách phòng.
    -   `RemoveRoom(int roomNumber)`: Xóa một phòng khỏi danh sách.
    -   `DisplayRoomList()`: Hiển thị danh sách các phòng trong khách sạn.
    -   `DisplayAvailableRoom(DateTime checkin, DateTime checkout)`: Hiển thị danh sách phòng có thể đặt được trong khoảng thời gian.
    -   `RevenueReport()`: Hiển thị doanh thu thu theo từng ngày.

### Class Booking (Đặt phòng):

-   Properties:
    -   `BookingId`: Mã số đặt phòng.
    -   `CheckInDate`: Ngày nhận phòng.
    -   `CheckOutDate`: Ngày nhận phòng.
    -   `CustomerName`: Tên của khách hàng.
    -   `TotalAmountPaid`: Số tiền khách hàng phải trả.
    -   `IsComplete`: Đơn hàng này đã hoàn thành ngày checkout đã bé hơn ngày hiện tại.

Lưu ý:

-   Source code mẫu đã được cung cấp. [Link source Code](https://gitlab.com/huyxvd/oop-hotel-capstone)
-   Xử lý ngoại lệ trong các hàm có thể gây ra lỗi.
-   Thông báo rõ ràng cho người dùng trong trường hợp xảy ra lỗi hoặc khi thực hiện các hành động.
-   Đảm bảo OOP được tuân thủ.